#
# @file imageresolver.py
#
# BreachSolver
# Copyright 2021 Eetu 'Devenec' Oinasmaa
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#

import concurrent.futures
import config
import cv2 as cv
import multiprocessing
import numpy

class ImageResolver:
	class Point:
		threshold = 10

		def __init__(self, location):
			self.x = location[0]
			self.y = location[1]

		def isYEqualTo(self, point):
			return abs(self.y - point.y) <= self.threshold

		def __eq__(self, point):
			return abs(self.x - point.x) <= self.threshold and abs(self.y - point.y) <= self.threshold

		def __lt__(self, point):
			if self.isYEqualTo(point):
				return self.x < point.x

			return self.y < point.y

	class Node:
		def __init__(self, code, location):
			self.code = code
			self.point = ImageResolver.Point(location)

		def __eq__(self, node):
			return self.point == node.point

		def __lt__(self, node):
			return self.point < node.point

	codes = [
		"1C",
		"55",
		"7A",
		"BD",
		"E9",
		"FF"
	]

	conversionThreshold    = 127
	templateBufferFilename = "templates/buffer_1440.png"
	templateNodesFilename  = "templates/nodes_1440.png"
	templateNativeHeight   = 1440

	def __init__(self):
		self.binaryImage = None
		self.greyImage = None
		self.imageNodeOffset = (0, 0)
		self.maxThreadCount = (multiprocessing.cpu_count() if config.maxThreadCount == 0 else
			config.maxThreadCount)

		self.templateBuffer = None
		self.templateNodes = {}
		self.templateNodesSmall = {}

	def nodeOffset(self):
		return self.imageNodeOffset

	def resolveMatrixAndSequences(self, image):
		self.initialiseTemplates(image.shape[0])
		self.initialiseImages(image)
		bufferSize = self.resolveBufferSize()
		matrixNodes, sequenceNodes = self.resolveNodes()

		return bufferSize, matrixNodes, sequenceNodes

	# Private

	def initialiseTemplates(self, imageHeight):
		scale = self.calculateTemplateScale(imageHeight)
		self.templateBuffer = self.loadTemplateImage(self.templateBufferFilename, scale)
		image = self.loadTemplateImage(self.templateNodesFilename, scale)
		self.templateNodes = self.sliceTemplates(image)
		imageSmall = self.resizeTemplatesImage(image)
		self.templateNodesSmall = self.sliceTemplates(imageSmall)

	def initialiseImages(self, image):
		croppedImage = self.cropImage(image)
		self.greyImage = cv.cvtColor(croppedImage, cv.COLOR_BGR2GRAY)
		threshold, self.binaryImage = cv.threshold(self.greyImage, self.conversionThreshold, 255,
			cv.THRESH_BINARY)

	def resolveBufferSize(self):
		match = cv.matchTemplate(self.greyImage, self.templateBuffer, cv.TM_CCOEFF_NORMED)
		locations = numpy.where(match >= config.matchThreshold)

		return len(locations[0]) if locations else 0

	def resolveNodes(self):
		matrixNodes = []
		sequenceNodes = []

		with concurrent.futures.ThreadPoolExecutor(self.maxThreadCount) as executor:
			futures = {executor.submit(self.matchTemplate, code, template): matrixNodes for code, template in
				self.templateNodes.items()}

			futures.update({executor.submit(self.matchTemplate, code, template): sequenceNodes for
				code, template in self.templateNodesSmall.items()})

			for future in concurrent.futures.as_completed(futures):
				futures[future] += future.result()

		matrixNodes.sort()
		sequenceNodes.sort()

		return matrixNodes, sequenceNodes

	def cropImage(self, image):
		width = config.imageCropRatios[0] * image.shape[1]
		height = config.imageCropRatios[1] * image.shape[0]
		x0 = int((image.shape[1] - width) / 2)
		x1 = image.shape[1] - x0
		y0 = int((image.shape[0] - height) / 2)
		y1 = image.shape[0] - y0
		self.setImageNodeOffset(x0, y0)

		return image[y0:y1, x0:x1]

	def matchTemplate(self, code, template):
		match = cv.matchTemplate(self.binaryImage, template, cv.TM_CCOEFF_NORMED)
		locations = numpy.where(match >= config.matchThreshold)
		nodes = []

		for location in zip(*locations[::-1]):
			node = ImageResolver.Node(code, location)

			if not node in nodes:
				nodes.append(node)

		return nodes

	def setImageNodeOffset(self, croppedImageOffsetX, croppedImageOffsetY):
		templateImage = next(iter(self.templateNodes.values()))
		offsetX = croppedImageOffsetX + int(templateImage.shape[1] / 2)
		offsetY = croppedImageOffsetY + int(templateImage.shape[0] / 2)
		self.imageNodeOffset = (offsetX, offsetY)

	@staticmethod
	def calculateTemplateScale(imageHeight):
		if imageHeight == ImageResolver.templateNativeHeight:
			return 1.0

		return imageHeight / ImageResolver.templateNativeHeight

	@staticmethod
	def loadTemplateImage(path, scale):
		image = cv.imread(path, 0)

		if image is None:
			raise Exception("Failed to load template image '{}'.".format(path))

		if scale == 1.0:
			return image

		width = round(scale * image.shape[1])
		height = round(scale * image.shape[0])

		return cv.resize(image, (width, height), cv.INTER_NEAREST)

	@staticmethod
	def sliceTemplates(image):
		codeCount = len(ImageResolver.codes)
		codeWidth = int(image.shape[1] / codeCount)
		templates = {}

		for i in range(0, codeCount):
			x = i * codeWidth
			templates[ImageResolver.codes[i]] = (image[0:, x : (x + codeWidth)])

		return templates

	@staticmethod
	def resizeTemplatesImage(image):
		width = round(config.sequenceNodeScale * image.shape[1])
		height = round(config.sequenceNodeScale * image.shape[0])
		codeCount = len(ImageResolver.codes)

		return cv.resize(image, (width, height), cv.INTER_NEAREST)
