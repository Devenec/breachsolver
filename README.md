# BreachSolver

A Cyberpunk 2077 Breach Protocol minigame solver  
Version 0.5.0


## About

The program is written in Python 3.


## How to Use

Python 3.7 is required. Earlier versions of Python 3 may work, but haven't been tested. See
[dependencies](#dependencies) for the list of dependencies and how to install them.

Run *breachsolver.py*, e.g. in command prompt / terminal:  
`> python breachsolver.py`

The program will wait for a key press (F8 by default) that will trigger the solving process. The program:
- takes a screenshot of the primary display,
- runs a template match to determine the buffer size, the code matrix and the code sequences,
- determines the best code sequence, and
- generates mouse events to input the code sequence

**IMPORTANT**: Please don't move the mouse or press any keys until the solving process is done.

Close the window to exit the program. The key setting and other settings can be found in *config.py*.


## Dependencies

- opencv-python
- pynput
- pywin32

Install dependencies with *pip* (located in *<Python installation directory>/Scripts*) in
command prompt / terminal:  
`> pip.exe install opencv-python pynput pywin32`


## Thanks

Joshua Chen's [Cyberpunk 2077 Autohacker](https://gitlab.com/jkchen2/cpah) helped to correctly
[generate mouse events](https://gitlab.com/jkchen2/cpah/-/blob/master/cpah/mouse_helper.py#L38).


## Known Issues

### The game window must be in the primary display in fullscreen mode

The program takes screenshots of the primary display. Both *Windowed Borderless* and *Fullscreen* work fine.

### The template matching is resolution dependent

The program has been tested properly on 1920 x 1080 and 2560 x 1440 resolution only. The program scales the
template images (*templates/buffer_1440.png* and *templates/nodes_1440.png*) to the size of the corresponding
elements in the game, if necessary. Template matching parameters can be tweaked in *config.py*.


## Copyright & License

Copyright 2021 Eetu 'Devenec' Oinasmaa  
Licensed under MIT License
