#
# @file debugger.py
#
# BreachSolver
# Copyright 2021 Eetu 'Devenec' Oinasmaa
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#

import cv2 as cv
import os
import time

class Debugger:
	debugDirectory           = "debug"
	screenshotFilenameFormat = "shot-{}.png"

	def __init__(self):
		if not os.path.isdir(self.debugDirectory):
			os.mkdir(self.debugDirectory)

	def writeScreenshot(self, image):
		timestamp = time.strftime("%Y-%m-%dT%H.%M.%S")
		filename = self.screenshotFilenameFormat.format(timestamp)
		cv.imwrite(os.path.join(self.debugDirectory, filename), image)
