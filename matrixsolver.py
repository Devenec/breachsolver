#
# @file matrixsolver.py
#
# BreachSolver
# Copyright 2021 Eetu 'Devenec' Oinasmaa
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#

import copy
import itertools
import math

class MatrixNode:
	def __init__(self, imageNode, row, column):
		self.code = imageNode.code
		self.imagePosition = None if imageNode is None else imageNode.point
		self.matrixPosition = (row, column)

	def __eq__(self, node):
		return self.code == node.code

	def __repr__(self):
		return self.code

class Matrix:
	def __init__(self, rows):
		self.rows = rows

	def dimension(self):
		return len(self.rows)

	def getColumn(self, index):
		return (row[index] for row in self.rows)

	def getRow(self, index):
		return self.rows[index]

class SequenceNode:
	wildcardCode = "**"

	def __init__(self, imageNode = None, code = ""):
		assert(not imageNode is None or code)

		self.code = code if code else imageNode.code

	def __eq__(self, node):
		return self.code == node.code

	def __repr__(self):
		return self.code

class Sequence:
	def __init__(self, score, nodes):
		self.nodes = nodes
		self.score = score

	def addNode(self, node):
		self.nodes.append(node)

	def wildcardCount(self):
		return sum(1 for node in self.nodes if node.code == SequenceNode.wildcardCode)

	def __eq__(self, sequence):
		return self.nodes == sequence.nodes

	def __len__(self):
		return len(self.nodes)

	def __lt__(self, sequence):
		if self.score == sequence.score:
			if len(self) == len(sequence):
				return self.wildcardCount() < sequence.wildcardCount()

			return len(self) < len(sequence)

		return sequence.score < self.score

	def __repr__(self):
		return repr(self.nodes)

class Solver:
	maxConsecutiveWildcardNodeCount = 2

	class SolveState:
		def __init__(self):
			self.position = [0, 0]
			self.positionIndex = 0
			self.sequence = []

		def inRow(self):
			return self.positionIndex == 0

		def intersect(self, position):
			solveState = copy.deepcopy(self)
			solveState.position = position
			solveState.positionIndex ^= 1

			return solveState

		def isNodeInSequence(self, node):
			return any(sequenceNode.matrixPosition == node.matrixPosition for sequenceNode in self.sequence)

	def __init__(self, bufferSize, matrix, sequences):
		self.bufferSize = bufferSize
		self.fullSequences = []
		self.matrix = matrix
		self.sequences = sequences

	def solve(self, debug):
		self.resolveFullSequences()

		if debug:
			self.debugPrintSequences()

		return self.findPassableSequence()

	# Private

	def resolveFullSequences(self):
		firstRow = self.matrix.getRow(0)

		for sequencePermutation in self.getSequencePermutations(self.sequences):
			self.removeSubSequences(sequencePermutation)
			optimised, optimisedSequence = self.optimiseToFullSequence(sequencePermutation)
			regularSequence = self.combineToFullSequence(sequencePermutation) if optimised else None

			if optimisedSequence.nodes[0] in firstRow:
				self.addFullSequences(optimisedSequence, regularSequence)

			self.addWildcardSequence(optimisedSequence)

			if not regularSequence is None:
				self.addWildcardSequence(regularSequence)

		self.fullSequences.sort()

	def findPassableSequence(self):
		print("")

		for sequence in self.fullSequences:
			print("Attempting sequence {}...".format(sequence))
			result = self.traverseMatrix(sequence, Solver.SolveState())

			if not result is None:
				return Sequence(sequence.score, result.sequence)

		return None

	def getSequencePermutations(self, sequences):
		permutations = []

		for i in range(len(sequences)):
			for permutation in itertools.permutations(sequences, i + 1):
				if len(permutation[0]) <= self.bufferSize:
					permutations.append([copy.copy(sequence) for sequence in permutation])

		return permutations

	def optimiseToFullSequence(self, sequences):
		optimisedSequence = sequences[0]
		optimised = False

		for sequence in sequences[1:]:
			overlap = next((i for i in range(len(sequence), 0, -1) if
				sequence.nodes[:i] == optimisedSequence.nodes[-i:]), 0)

			if self.bufferSize < len(optimisedSequence) + len(sequence) - overlap:
				break

			optimisedSequence = self.combineSequences(optimisedSequence, sequence, overlap)

			if overlap:
				optimised = True

		return optimised, optimisedSequence

	def combineToFullSequence(self, sequences):
		fullSequence = sequences[0]

		for sequence in sequences[1:]:
			if self.bufferSize < len(fullSequence) + len(sequence):
				break

			fullSequence = self.combineSequences(fullSequence, sequence, 0)

		return fullSequence

	def addFullSequences(self, sequenceA, sequenceB):
		self.addFullSequence(sequenceA)

		if not sequenceB is None:
			self.addFullSequence(sequenceB)

	def addWildcardSequence(self, sequence):
		wildcardNodeCount = min(self.maxConsecutiveWildcardNodeCount, self.bufferSize - len(sequence))

		for i in range(wildcardNodeCount):
			nodes = ((i + 1) * [SequenceNode(code = SequenceNode.wildcardCode)]) + sequence.nodes
			self.addFullSequence(Sequence(sequence.score, nodes))

	def traverseMatrix(self, sequence, parentSolveState):
		for node in self.getMatchingNodes(sequence, parentSolveState):
			solveState = parentSolveState.intersect(node.matrixPosition)
			solveState.sequence.append(node)

			if len(solveState.sequence) == len(sequence):
				return solveState

			result = self.traverseMatrix(sequence, solveState)

			if not result is None:
				return result

		return None

	def addFullSequence(self, fullSequence):
		existingSequence = next((sequence for sequence in self.fullSequences if sequence == fullSequence),
			None)

		if existingSequence is None:
			self.fullSequences.append(fullSequence)
		else:
			existingSequence.score = max(existingSequence.score, fullSequence.score)

	def getMatchingNodes(self, sequence, solveState):
		position = solveState.position
		nodes = self.matrix.getRow(position[0]) if solveState.inRow() else self.matrix.getColumn(position[1])
		targetNode = sequence.nodes[len(solveState.sequence)]

		if targetNode.code == SequenceNode.wildcardCode:
			return (node for node in nodes if not solveState.isNodeInSequence(node))
		else:
			return (node for node in nodes if node == targetNode and not solveState.isNodeInSequence(node))

	def debugPrintSequences(self):
		print("\nPossible full sequences:")

		for sequence in self.fullSequences:
			print("{0} (score {0.score})".format(sequence))

	@staticmethod
	def removeSubSequences(sequences):
		i = 0

		while i < len(sequences):
			j = i + 1

			while j < len(sequences):
				if Solver.isSequenceInSequence(sequences[j], sequences[i]):
					sequences[i].score += sequences[j].score
					sequences.pop(j)
				else:
					j += 1

			i += 1

	@staticmethod
	def combineSequences(sequenceA, sequenceB, overlap):
		return Sequence(sequenceA.score + sequenceB.score, sequenceA.nodes + sequenceB.nodes[overlap:])

	@staticmethod
	def isSequenceInSequence(subsequence, sequence):
		for i in range(len(sequence) - len(subsequence) + 1):
			if sequence.nodes[i : i + len(subsequence)] == subsequence.nodes:
				return True

		return False

class MatrixSolver:
	def __init__(self):
		self.matrix = None
		self.sequenceCount = 0

	def getSequenceIndices(self, sequence):
		return [i + 1 for i in range(self.sequenceCount) if ((1 << i) & sequence.score) != 0]

	def printResultMatrix(self, result):
		matrix = list(["  "] * self.matrix.dimension() for row in self.matrix.rows)

		for i in range(len(result)):
			node = result.nodes[i]
			matrix[node.matrixPosition[0]][node.matrixPosition[1]] = " {}".format(i)

		for row in matrix:
			print("[{}]".format(", ".join(row)))

	def solve(self, bufferSize, matrixNodes, sequenceNodes, debug):
		print("Buffer size: {}".format(bufferSize))
		self.matrix = self.createMatrix(matrixNodes)
		sequences = self.createSequences(sequenceNodes)
		solver = Solver(bufferSize, self.matrix, sequences)

		return solver.solve(debug)

	# Private

	def createSequences(self, sequenceNodes):
		sequences = MatrixSolver.createSequenceList(sequenceNodes)
		self.sequenceCount = len(sequences)
		MatrixSolver.iterateSequencesForDuplicates(sequences)

		return sequences

	@staticmethod
	def createMatrix(matrixNodes):
		dimension = MatrixSolver.determineMatrixDimension(matrixNodes)
		rows = []
		print("\nMatrix:")

		for i in range(dimension):
			nodes = []

			for j in range(dimension):
				nodes.append(MatrixNode(matrixNodes[i * dimension + j], i, j))

			rows.append(nodes)
			print(nodes)

		return Matrix(rows)

	@staticmethod
	def determineMatrixDimension(matrixNodes):
		dimension = math.sqrt(len(matrixNodes))

		if math.modf(dimension)[0] != 0:
			raise RuntimeError(
				"Failed to recognise all matrix nodes, as the recognised matrix is not a square matrix.")

		return int(dimension)

	@staticmethod
	def createSequenceList(sequenceNodes):
		imagePosition = sequenceNodes[0].point
		sequence = Sequence(1, [SequenceNode(sequenceNodes[0])])
		sequences = []

		for node in sequenceNodes[1:]:
			if node.point.isYEqualTo(imagePosition):
				sequence.addNode(SequenceNode(node))
			else:
				sequences.append(sequence)
				sequence = Sequence(1 << len(sequences), [SequenceNode(node)])
				imagePosition = node.point

		sequences.append(sequence)
		return sequences

	@staticmethod
	def iterateSequencesForDuplicates(sequences):
		print("\nSequences:")
		i = 0

		while i < len(sequences):
			duplicateSequence = next((sequence for sequence in sequences[i + 1:] if sequence == sequences[i]),
				None)

			removedString = "" if duplicateSequence is None else ", removed as duplicate"
			print("{0} (score {0.score}{1})".format(sequences[i], removedString))

			if duplicateSequence is None:
				i += 1
			else:
				duplicateSequence.score += sequences[i].score
				sequences.pop(i)
