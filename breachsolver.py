#
# @file breachsolver.py
#
# BreachSolver
# Copyright 2021 Eetu 'Devenec' Oinasmaa
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#

import config
import debugger
import imageresolver
import input
import matrixsolver
import screenshot
import time

from pynput import keyboard

version = "0.5.0"

class Solver:
	def __init__(self):
		self.debugger = debugger.Debugger() if config.debug else None
		self.screenshot = screenshot.Screenshot(self.debugger)
		self.imageResolver = imageresolver.ImageResolver()
		self.matrixSolver = matrixsolver.MatrixSolver()
		self.input = input.Input()

	def solve(self):
		self.input.resetCursor(self.screenshot.screenResolution())

		try:
			image = self.screenshot.capture()
		except RuntimeError as error:
			self.printError(error)
			return

		bufferSize, matrixNodes, sequenceNodes = self.imageResolver.resolveMatrixAndSequences(image)
		result = self.validateImageResolveOutput(bufferSize, matrixNodes, sequenceNodes)

		if not result:
			return

		try:
			result = self.matrixSolver.solve(bufferSize, matrixNodes, sequenceNodes, config.debug)
		except RuntimeError as error:
			self.printError(error)
			return

		self.processResult(result)

	# Private

	def processResult(self, result):
		self.printResult(result)

		if result is None:
			return

		self.input.processResult(result, self.imageResolver.nodeOffset())

	def printResult(self, result):
		if result is None:
			print("\nFAILED TO FIND PASSABLE SEQUENCES")
		else:
			sequenceIndices = self.matrixSolver.getSequenceIndices(result)
			print("\nSUCCESS:\n{} includes sequences {}\n".format(result, sequenceIndices))
			self.matrixSolver.printResultMatrix(result)

		print("\n")

	@staticmethod
	def validateImageResolveOutput(bufferSize, matrixNodes, sequenceNodes):
		if bufferSize == 0:
			Solver.printError("Failed to resolve the buffer size from the screenshot.")
			return False

		if not (matrixNodes and sequenceNodes):
			Solver.printError("Failed to resolve the matrix and/or sequences from the screenshot.")
			return False

		return True

	@staticmethod
	def printError(error):
		print("ERROR: {}".format(error))

if __name__ == "__main__":
	print("BreachSolver {}\nCopyright 2021 Eetu 'Devenec' Oinasmaa\n".format(version))

	if not type(config.key) in (str, keyboard.Key):
		raise RuntimeError("config.key is of invalid type.")

	infoFormat = (
		"Press '{}' to solve the Breach Protocol. The program takes a screenshot of\n"
		"the primary display, determines the buffer size, the code matrix and the code\n"
		"sequences, determines the best code sequence, and generates mouse events to\n"
		"input the code sequence.\n\n"
		"IMPORTANT: Please don't move the mouse or press any keys until the solving\n"
		"process is done.\n\n"
		"Close this window to exit the program.\n"
	)

	print(infoFormat.format(config.key))
	solver = Solver()

	while True:
		if input.Input.processKeyEvents():
			solver.solve()
