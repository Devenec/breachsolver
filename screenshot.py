#
# @file screenshot.py
#
# BreachSolver
# Copyright 2021 Eetu 'Devenec' Oinasmaa
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#

import cv2 as cv
import numpy
import pywintypes
import win32con
import win32gui
import win32ui

class Screenshot:
	def __init__(self, debugger):
		self.debugger = debugger
		self.windowHandle = win32gui.GetDesktopWindow()
		self.windowRectangle = None

	def capture(self):
		self.updateWindowRectangle()

		try:
			windowDeviceContext = win32gui.GetDC(self.windowHandle)

			try:
				wrapperDeviceContext, captureDeviceContext = self.createDeviceContexts(windowDeviceContext)

				try:
					image = self.captureImage(wrapperDeviceContext, captureDeviceContext)
				except win32ui.error as error:
					raise error
				finally:
					captureDeviceContext.DeleteDC()
			except win32ui.error as error:
				raise error
			finally:
				win32gui.ReleaseDC(self.windowHandle, windowDeviceContext)
		except (pywintypes.error, win32ui.error) as error:
			raise RuntimeError("Failed to capture a screenshot ({}).".format(error))

		if not self.debugger is None:
			self.debugger.writeScreenshot(image)

		return image

	def screenResolution(self):
		self.updateWindowRectangle()
		return (self.windowRectangle[2], self.windowRectangle[3])

	# Private

	def updateWindowRectangle(self):
		self.windowRectangle = win32gui.GetClientRect(self.windowHandle)

	def captureImage(self, wrapperDeviceContext, captureDeviceContext):
		captureBitmap = win32ui.CreateBitmap()
		captureBitmap.CreateCompatibleBitmap(wrapperDeviceContext, self.windowRectangle[2],
			self.windowRectangle[3])

		try:
			originalBitmap = captureDeviceContext.SelectObject(captureBitmap)
			captureSize = (self.windowRectangle[2], self.windowRectangle[3])
			sourcePosition = (self.windowRectangle[0], self.windowRectangle[1])
			captureDeviceContext.BitBlt((0, 0), captureSize, wrapperDeviceContext, sourcePosition,
				win32con.SRCCOPY)

			image = self.createImage(captureBitmap)
			captureDeviceContext.SelectObject(originalBitmap)
		except win32ui.error as error:
			raise error
		finally:
			win32gui.DeleteObject(captureBitmap.GetHandle())

		return image

	@staticmethod
	def createDeviceContexts(windowDeviceContext):
		wrapperDeviceContext = win32ui.CreateDCFromHandle(windowDeviceContext)
		return wrapperDeviceContext, wrapperDeviceContext.CreateCompatibleDC()

	@staticmethod
	def createImage(bitmap):
		bitmapInfo = bitmap.GetInfo()

		if bitmapInfo["bmBitsPixel"] != 32:
			raise win32ui.error("The desktop color depth is not 32 bits")

		image = numpy.array(bitmap.GetBitmapBits(), dtype = numpy.uint8)
		return image.reshape(bitmapInfo["bmHeight"], bitmapInfo["bmWidth"], 4)
