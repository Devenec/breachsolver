#
# @file config.py
#
# BreachSolver
# Copyright 2021 Eetu 'Devenec' Oinasmaa
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#

from pynput.keyboard import Key

# Defines the key that triggers the solving process
#
# Must be a character (e.g. key = "a") or one of "Key" constants listed here:
# https://pynput.readthedocs.io/en/latest/keyboard.html#pynput.keyboard.Key
key = Key.f8

# Defines the ratios used to crop screenshots for faster template matching
#
# The ratios defin the width and the height, respectively, of the cropped image
# in percentage to the original screenshot. Both values must be in range [0, 1].
imageCropRatios = (0.8, 0.7)

# Defines the threshold used in template matching
#
# Higher values are more strict. Must be in range [0, 1].
matchThreshold = 0.8

# Defines the maximum number of threads used in node template matching
#
# When set to 0, the number of CPUs in the system is used.
maxThreadCount = 0

# Defines the interval, in seconds, in which mouse cursor moves and clicks are
# generated in the solving process
mouseEventInterval = 0.2

# Defines the scale of node template matching image for sequence codes
# (codes on the right side of Breach Protocol UI)
sequenceNodeScale = 0.84

# Defines whether to enable debugging
#
# Screenshots will be written into "debug" directory
debug = False
