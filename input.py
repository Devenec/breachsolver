#
# @file input.py
#
# BreachSolver
# Copyright 2021 Eetu 'Devenec' Oinasmaa
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#

import config
import ctypes
import time

from pynput import keyboard

MOUSEEVENTF_MOVE     = 0x0001
MOUSEEVENTF_LEFTDOWN = 0x0002
MOUSEEVENTF_LEFTUP   = 0x0004
MOUSEEVENTF_ABSOLUTE = 0x8000

class Input:
	mouseEventClick = MOUSEEVENTF_LEFTDOWN + MOUSEEVENTF_LEFTUP
	mouseEventMove = MOUSEEVENTF_MOVE + MOUSEEVENTF_ABSOLUTE

	def __init__(self):
		self.screenResolution = None

	def processResult(self, result, positionOffset):
		for node in result.nodes:
			position = (positionOffset[0] + node.imagePosition.x, positionOffset[1] + node.imagePosition.y)
			self.clickAtPosition(position)

	def resetCursor(self, resolution):
		self.screenResolution = resolution
		self.presetCursorPosition()
		time.sleep(config.mouseEventInterval)

	@staticmethod
	def processKeyEvents():
		with keyboard.Events() as events:
			event = events.get()

			if type(event) is keyboard.Events.Release:
				return event.key in [config.key, keyboard.KeyCode.from_char(config.key)]

		return False

	# Private

	def clickAtPosition(self, position):
		self.presetCursorPosition()
		time.sleep(0.01)
		self.setCursorPosition(position)
		time.sleep(0.01)
		self.sendMouseEvent(self.mouseEventClick)
		time.sleep(config.mouseEventInterval)

	def presetCursorPosition(self):
		self.setCursorPosition((99999, 99999))
		time.sleep(0.01)
		self.setCursorPosition((0, 0))

	def setCursorPosition(self, position):
		x = int(position[0] / self.screenResolution[0] * 65535)
		y = int(position[1] / self.screenResolution[1] * 65535)
		self.sendMouseEvent(self.mouseEventMove, x, y)

	@staticmethod
	def sendMouseEvent(event, x = 0, y = 0):
		ctypes.windll.user32.mouse_event(event, x, y, 0, 0)
